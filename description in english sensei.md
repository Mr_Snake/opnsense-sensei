# **HERE IS A BRIEF DESCRIPTION OF SENSEI**

**What is sensei?**

The technology behind Sensei is the industry's first package inspection engine that can perform native TLS inspection.
Sensei technology offers cybersecurity tools with visibility, packet classification and precise policy application for all types of traffic.
Greater package intelligence means better decision making. Better decision making means better success rates in detecting and preventing cyber attacks.
There is excellent package intelligence so you can take advantage of cybersecurity tools.
What function does this tool provide?

# 1.Visibility and control of applications

Block or control unauthorized or misbehaving applications, regardless of port numbers.
A rich application database consists of signatures for thousands of applications.

# 2. Downlink visibility of the network

View your network in real time.
Start with an overview; explore the details by connection.
Visually detect anomalies.
A rich application database identified with thousands of communication protocols and data attributes,
the most accurate image of data activity in real time.

# 3.Filtering and saved user reports

Easily integrate with Microsoft Active Directory or Captive Portal to have reports and filtering associated with users.
Detect deviations from normal user behavior.

# 4.Web security and cloud application controls

Apply web filtering policies for more than 140 million websites in more than 120 different web categories.
Create custom categories to blacklist or whitelist sites.
With Cloud Application Controls, you can create granular access policies for cloud services like Google, Dropbox, Amazon, Twitter, etc.
The commercial grade cloud-based web categorization database provides real-time classification for unknown sites under 5 minutes.
New botnets are detected and blocked in minutes.

# 5.Visibility and TLS control

The bad guys are hiding under encryption.
Enable 100% transparent TLS inspection for all TCP ports with one click.
Fully integrated with the firewall certificate manager, you can create a completely new
certification authority certificate or importer the certification authority keys produced by your company.