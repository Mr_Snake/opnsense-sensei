# **VOICI UNE BREF DESCRIPTION DE SENSEI**

**Qu'est ce que sensei ?**

La technologie derrière Sensei est le premier moteur d'inspection de paquets de l'industrie qui peut effectuer une inspection TLS native.
La technologie Sensei offre des outils de cybersécurité avec une visibilité, une classification des paquets et une application de politique précise pour tout type de trafic.
Une plus grande intelligence des paquets signifie une meilleure prise de décision. Une meilleure prise de décision signifie de meilleurs taux de réussite dans la détection et la prévention des cyberattaques.
Il avons on une excellente intelligence des paquets afin que vous puissiez profiter d'excellents outils de cybersécurité.


**Quelle fonction apporte cette outils ?**

# 1.Visibilité et contrôle des applications

Bloquer ou contrôler les applications non autorisées ou qui se comportent mal, quels que soient les numéros de port.
Une base de données d'applications riche se compose de signatures pour des milliers d'applications.

# 2.Visibilité descendante du réseau

Visualisez votre réseau en temps réel.
Commencez par une vue d'ensemble; explorer les détails par connexion.
Détectez visuellement les anomalies.
Une base de données d'applications riche identifie des milliers de protocoles de communication et d'attributs de données, 
créant l'image la plus précise de l'activité des données en temps réel.

# 3.Filtrage et rapports basés sur l'utilisateur

Intégrez facilement avec Microsoft Active Directory ou Captive Portal pour avoir des rapports et un filtrage basés sur les utilisateurs.
Détectez les écarts par rapport au comportement normal de l'utilisateur.

# 4.Sécurité Web et contrôles des applications cloud

Appliquez des politiques de filtrage Web pour plus de 140 millions de sites Web dans plus de 120 catégories Web différentes.
Créez des catégories personnalisées pour mettre des sites sur liste noire ou sur liste blanche.
Avec Cloud Application Controls, vous pouvez créer des politiques d'accès granulaires pour les services Cloud comme Google, Dropbox, Amazon, Twitter, etc.
La base de données de catégorisation Web basée sur le cloud de qualité commerciale fournit une classification en temps réel pour les sites inconnus de moins de 5 minutes.
De nouveaux botnets sont détectés et bloqués en quelques minutes.

# 5.Visibilité et contrôle TLS

Les méchants se cachent sous cryptage.
Activez l'inspection TLS 100% transparente pour tous les ports TCP en un seul clic.
Entièrement intégré au gestionnaire de certificats de pare-feu, vous pouvez créer un tout nouveau 
certificat d'autorité de certification ou importer les clés d'autorité de certification existantes de votre entreprise.